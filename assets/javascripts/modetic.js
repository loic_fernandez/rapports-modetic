/**
 * Created by loic on 30/08/2018.
 */

$(document).ready(function() {

    $('.input-daterange').datepicker({
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });

    $('.input-date-month').datepicker({
        todayHighlight: false,
        format: 'yyyy-mm',
        defaultViewDate: "months",
        minViewMode: "months",
        language: "fr"
    });

    $('#dataTable').DataTable({
        "scrollX": true,
        "pageLength": 50
    });

    $(".js-expand").on("click", function (e) {
        e.preventDefault();
        $(".sidebar").addClass("expanded").removeClass("collapsed");
        $(this).toggleClass("d-none");
        $(".js-reduce").toggleClass("d-none");
    });

    $(".js-reduce").on("click", function (e) {
        e.preventDefault();
        $(".sidebar").removeClass("expanded").addClass("collapsed");
        $(this).toggleClass("d-none");
        $(".js-expand").toggleClass("d-none");
    });

    $('[data-toggle="tooltip"]').tooltip()

});
