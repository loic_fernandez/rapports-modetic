<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PDO;


class ReportController extends AbstractController
{
    /**
     * @Route("/", name="report")
     */
    public function index()
    {
        return $this->render('report/index.html.twig');
    }

	/**
	 * @Route("/report/comptable", name="comptable")
	 */
    public function comptableIndex() {
		$request = Request::createFromGlobals();

		$from  = $request->request->get('from', date("Y-m-01")) . " 00:00:00";
		$to  = $request->request->get('to', date("Y-m-d")) . " 23:59:59";
		$dates = $this->formatFromTo($from, $to);

		$data = $this->getComptableReport($dates["from"], $dates["to"]);

		return $this->render('report/comptable/index.html.twig', [
		'data' => $data,
		'from' => $from,
		'to' => $to,
		]);
	}

    /**
     * @Route("/report/comptable/export/from/{from}/to/{to}", name="exportComptable")
     */
    public function exportComptable($from, $to) {

        $dates = $this->formatFromTo($from, $to);
        $data = $this->getReport($dates["from"], $dates["to"]);

        $response = new Response();
        $response->setContent("" . $this->renderView('report/comptable/export.csv.twig', [
            'data' => $data,
        ]));
        $response->setCharset("utf-8");

        $response->headers->set('Content-Type', 'application/csv;');
        $response->headers->set('Content-Disposition', 'attachment; filename=rapport_comptable.csv');
        $response->headers->set('Pragma', 'no-cache');

        return $response;

    }

    protected function getComptableReport($from, $to) {

        $db = $this->getPDO();
        $baseQuery = $db->query(<<<SQL
SELECT DISTINCT `core_store`.`name` AS `store_name`,
                `dc`.`fr_name`,
                `dc`.`ue`,
                `dc`.`country_id`,
                `sfi`.`updated_at` AS `date`,
                `sfi`.`entity_id`,
                `sfii`.`entity_id` AS `sfii_entity_id`,
                `sfi`.`increment_id`,
                CONCAT('facture') AS `type`,
                `sfi`.`grand_total` AS `total`,
                SUM(IF(STRCMP(cpf.type_value, 'Fini'), 0, 1)) AS `qty_fini`,
                SUM(IF(STRCMP(cpf.type_value, 'Manufacturé'), 0, 1)) AS `qty_man`,
                (sfi.shipping_amount > 0) AS `qty_fdp`,
                (sfi.giftcert_amount > 0) AS `qty_giftcert`,
                IF(STRCMP(sfop.method, 'cybermut_payment')
                   AND STRCMP(sfop.method, 'BankTerminal'), 0, sfi.grand_total) AS `total_paid_cb`,
                IF(STRCMP(sfop.method, 'banktransfer'), 0, sfi.grand_total) AS `total_paid_vir`,
                IF(STRCMP(sfop.method, 'Money'), 0, sfi.grand_total) AS `total_paid_ca`,
                IF(STRCMP(sfop.method, 'Check')
                   AND STRCMP(sfop.method, 'checkmo'), 0, sfi.grand_total) AS `total_paid_ch`,
                IF(STRCMP(sfop.method, 'paypal_standard')
                   AND STRCMP(sfop.method, 'paypal_mep')
                   AND STRCMP(sfop.method, 'hosted_pro'), 0, sfi.grand_total) AS `total_paid_pa`,
                IF(STRCMP(sfop.method, 'Mlc'), 0, sfi.grand_total) AS `total_paid_mlc`,
                IF(STRCMP(sfop.method, 'Partners'), 0, sfi.grand_total) AS `total_paid_partners`,
                IF(STRCMP(sfop.method, 'ugiftcert'), 0, sfi.grand_total) + SUM(sfi.base_giftcert_amount) AS `total_paid_ugiftcert`,
                IF(STRCMP(sfoa.country_id, 'fr'), 0, sfi.subtotal) AS `total_ca_fr`,
                IF(dc.ue AND sfoa.country_id != "fr", sfi.subtotal, 0) AS `total_ca_ue`,
                IF(dc.ue, 0, sfi.subtotal) AS `total_ca_autre`,
                IF(STRCMP(sfoa.country_id, 'fr'), 0, sfi.shipping_amount) AS `total_fdp_fr`,
                IF(dc.ue AND sfoa.country_id != "fr", sfi.shipping_amount, 0) AS `total_fdp_ue`,
                IF(dc.ue, 0, sfi.shipping_amount) AS `total_fdp_autre`,
                IF(STRCMP(sfoa.country_id, 'fr'), 0, sfi.tax_amount) AS `total_tax_fr`,
                IF(dc.ue AND sfoa.country_id != "fr", sfi.tax_amount, 0) AS `total_tax_ue`,
                IF(dc.ue, 0, sfi.tax_amount) AS `total_tax_autre`
FROM `sales_flat_invoice` AS `sfi`
LEFT JOIN `core_store` ON core_store.store_id = sfi.store_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfi.shipping_address_id
LEFT JOIN `sales_flat_invoice_item` AS `sfii` ON sfii.parent_id = sfi.entity_id
LEFT JOIN `catalog_product_flat_1` AS `cpf` ON sfii.product_id = cpf.entity_id
LEFT JOIN `sales_flat_order_payment` AS `sfop` ON sfi.order_id = sfop.parent_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id
WHERE (sfi.updated_at >= '$from')
  AND (sfi.updated_at <= '$to')
  AND (sfii.row_total)
GROUP BY `sfi`.`entity_id`
UNION
SELECT DISTINCT `core_store`.`name` AS `store_name`,
                `dc`.`fr_name`,
                `dc`.`ue`,
                `dc`.`country_id`,
                `sfc`.`updated_at` AS `date`,
                `sfc`.`entity_id`,
                `sfci`.`entity_id` AS `sfii_entity_id`,
                `sfc`.`increment_id`,
                CONCAT('avoir') AS `type`,
                (0 - sfc.grand_total) AS `total`,
                SUM(IF(STRCMP(cpf.type_value, 'Fini'), 0, 1)) AS `qty_fini`,
                SUM(IF(STRCMP(cpf.type_value, 'Manufacturé'), 0, 1)) AS `qty_man`,
                (sfc.shipping_amount > 0) AS `qty_fdp`,
                (sfc.giftcert_amount > 0) AS `qty_giftcert`,
                0 - IF(STRCMP(sfop.method, 'cybermut_payment')
                   AND STRCMP(sfop.method, 'BankTerminal'), 0, sfc.grand_total) AS `total_paid_cb`,
                0 - IF(STRCMP(sfop.method, 'banktransfer'), 0, sfc.grand_total) AS `total_paid_vir`,
                0 - IF(STRCMP(sfop.method, 'Money'), 0, sfc.grand_total) AS `total_paid_ca`,
                0 - IF(STRCMP(sfop.method, 'Check')
                   AND STRCMP(sfop.method, 'checkmo'), 0, sfc.grand_total) AS `total_paid_ch`,
                0 - IF(STRCMP(sfop.method, 'paypal_standard')
                   AND STRCMP(sfop.method, 'paypal_mep')
                   AND STRCMP(sfop.method, 'hosted_pro'), 0, sfc.grand_total) AS `total_paid_pa`,
                0 - IF(STRCMP(sfop.method, 'Mlc'), 0, sfc.grand_total) AS `total_paid_mlc`,
                0 - IF(STRCMP(sfop.method, 'Partners'), 0, sfc.grand_total) AS `total_paid_partners`,
                0 - IF(STRCMP(sfop.method, 'ugiftcert'), 0, sfc.grand_total) + SUM(sfc.base_giftcert_amount) AS `total_paid_ugiftcert`,
                0 - IF(STRCMP(sfoa.country_id, 'fr'), 0, sfc.subtotal) AS `total_ca_fr`,
                0 - IF(dc.ue AND sfoa.country_id != "fr", sfc.subtotal, 0) AS `total_ca_ue`,
                0 - IF(dc.ue, 0, sfc.subtotal) AS `total_ca_autre`,
                0 - IF(STRCMP(sfoa.country_id, 'fr'), 0, sfc.shipping_amount) AS `total_fdp_fr`,
                0 - IF(dc.ue AND sfoa.country_id != "fr", sfc.shipping_amount, 0) AS `total_fdp_ue`,
                0 - IF(dc.ue, 0, sfc.shipping_amount) AS `total_fdp_autre`,
                0 - IF(STRCMP(sfoa.country_id, 'fr'), 0, sfc.tax_amount) AS `total_tax_fr`,
                0 - IF(dc.ue AND sfoa.country_id != "fr", sfc.tax_amount, 0) AS `total_tax_ue`,
                0 - IF(dc.ue, 0, sfc.tax_amount) AS `total_tax_autre`
FROM `sales_flat_creditmemo` AS `sfc`
LEFT JOIN `core_store` ON core_store.store_id = sfc.store_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfc.shipping_address_id
LEFT JOIN `sales_flat_creditmemo_item` AS `sfci` ON sfci.parent_id = sfc.entity_id
LEFT JOIN `catalog_product_flat_1` AS `cpf` ON sfci.product_id = cpf.entity_id
LEFT JOIN `sales_flat_order_payment` AS `sfop` ON sfc.order_id = sfop.parent_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id
WHERE (sfc.updated_at >= '$from')
  AND (sfc.updated_at <= '$to')
GROUP BY `sfc`.`entity_id`
SQL
        );

        $data = [];

        foreach($baseQuery as $q) {

            $tables = [
                "avoir" => [
                    "main_table" => "sales_flat_creditmemo",
                    "item" => "sales_flat_creditmemo_item",
                    "select_prefix" => "0 - "
                ],
                "facture" => [
                    "main_table" => "sales_flat_invoice",
                    "item" => "sales_flat_invoice_item",
                    "select_prefix" => ""
                ]
            ];

            $queries = [];
            $types = ["fini" => "Fini", "man" => "Manufacturé"];
            foreach ($types as $key => $type) {

                $index = "total_ca_" . $key;
                $index_fr = $index . "_fr";
                $index_ue = $index . "_ue";
                $index_autre = $index . "_autre";


                $q[$index_fr] = 0;
                $q[$index_ue] = 0;
                $q[$index_autre] = 0;

                $queries[$key] = $db->query(sprintf(<<<SQL
SELECT
  %sSUM(IF(sfoa.country_id = "fr", item.row_total, 0)) AS `%s`,
  %sSUM(IF(dc.ue AND sfoa.country_id != "fr", item.row_total, 0)) AS `%s`,
  %sSUM(IF(dc.ue, 0, item.row_total)) AS `%s`,
  `main_table`.`entity_id`
FROM `%s` AS `main_table` 
LEFT JOIN `%s` AS `item` ON item.parent_id = main_table.entity_id
LEFT JOIN `catalog_product_flat_1` AS `cpf` ON item.product_id = cpf.entity_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = main_table.shipping_address_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id
WHERE main_table.entity_id = %s
AND cpf.type_value = '%s'
SQL
                    ,
                    $tables{$q["type"]}["select_prefix"],
                    $index_fr,
                    $tables{$q["type"]}["select_prefix"],
                    $index_ue,
                    $tables{$q["type"]}["select_prefix"],
                    $index_autre,
                    $tables{$q["type"]}["main_table"],
                    $tables{$q["type"]}["item"],
                    $q["entity_id"],
                    $type
                ));
            }

            foreach ($queries as $i => $query) {
                if ($query) {
                    foreach($query as $q2) {
                        $q["total_ca_".$i."_fr"] = $q2["total_ca_".$i."_fr"];
                        $q["total_ca_".$i."_ue"] = $q2["total_ca_".$i."_ue"];
                        $q["total_ca_".$i."_autre"] = $q2["total_ca_".$i."_autre"];
                    }
                }

            }

            $data[] =  $q;
        }


        return $data;
    }



	/**
	 * @Route("/report/bilan", name="bilan")
	 */
	public function bilanIndex() {
		$request = Request::createFromGlobals();

		$from  = $request->request->get('from', date("Y-m-01")) . " 00:00:00";
		//$from  = "2017-05-05 00:00:00";
		$to  = $request->request->get('to', date("Y-m-d")) . " 23:59:59";
		//$to  = "2017-05-05 23:59:59";
		$dates = $this->formatFromTo($from, $to);

		$data = $this->getBilanReport($dates["from"], $dates["to"]);

		return $this->render('report/bilan/index.html.twig', [
			'data' => $data,
			'from' => $from,
			'to' => $to,
		]);
	}

	/**
	 * @Route("/report/bilan/export/from/{from}/to/{to}", name="exportBilan")
	 */
	public function exportBilan($from, $to) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getBilanReport($dates["from"], $dates["to"]);

		$response = new Response();
		$response->setContent("" . $this->renderView('report/bilan/export.csv.twig', [
				'data' => $data,
			]));
		$response->setCharset("utf-8");

		$response->headers->set('Content-Type', 'application/csv;');
		$response->headers->set('Content-Disposition', 'attachment; filename=rapport_bilan.csv');
		$response->headers->set('Pragma', 'no-cache');

		return $response;

	}

	protected function getBilanReport($from, $to) {

		$db = $this->getPDO();
		$baseQuery = $db->query(<<<SQL
SELECT
	core_store.name AS store_name,
	`dc`.`fr_name`,
    `dc`.`country_id`,
    `sfo`.`created_at` AS `date`,
    `sfo`.`entity_id`,
    `sfo`.`increment_id`,
    `item`.`item_id`,
    `item`.`name`,
    `item`.`sku`,
    `item`.`price_incl_tax`,
    `item`.`original_price`,
    `sfi`.`increment_id` as `invoice_id`,
    `sfi`.`updated_at` AS `invoice_date`,
	eaov.value as type_value
	
FROM sales_flat_order as sfo
LEFT JOIN sales_flat_order_item as item ON (item.order_id = sfo.entity_id AND item.parent_item_id IS NULL)
LEFT JOIN sales_flat_order_item as item2 ON (item2.order_id = sfo.entity_id AND item2.parent_item_id IS NOT NULL AND item2.parent_item_id = item.item_id AND item.product_type IN ('configurable', 'bundle'))
LEFT JOIN sales_flat_invoice as sfi ON sfi.order_id = sfo.entity_id
LEFT JOIN core_store ON core_store.store_id = sfo.store_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfo.shipping_address_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id

LEFT JOIN catalog_product_entity as cpe ON cpe.entity_id = item.product_id
LEFT JOIN eav_attribute as ea ON ea.attribute_code = "type"
LEFT JOIN catalog_product_entity_varchar as cpev ON cpev.attribute_id = ea.attribute_id AND cpev.entity_id = cpe.entity_id
LEFT JOIN catalog_product_entity_int as cpei ON cpei.attribute_id = ea.attribute_id AND cpei.entity_id = cpe.entity_id
LEFT JOIN eav_attribute_option_value as eaov ON eaov.option_id = IFNULL(cpei.value, cpev.value) AND eaov.store_id = 0


WHERE (sfo.created_at >= '$from')
  AND (sfo.created_at <= '$to')
  AND (item.qty_ordered - item.qty_refunded) > 0
  AND (((sfo.status = 'closed') OR (sfo.status = 'complete')))

GROUP BY item.item_id
SQL
		);

		$data = [];

		foreach($baseQuery as $q) {
			$data[] = [
				"store_name" => $q["store_name"],
				"fr_name" => $q["fr_name"],
				"country_id" => $q["country_id"],
				"date" => $q["date"],
				"item_id" => $q["item_id"],
				"increment_id" => $q["increment_id"],
				"invoice_id" => $q["invoice_id"],
				"invoice_date" => $q["invoice_date"],
				"name" => $q["name"],
				"sku" => $q["sku"],
				"price_incl_tax" => $q["price_incl_tax"],
				"original_price" => $q["original_price"],
				"type_value" => $q["type_value"]
			];
		}


		return $data;
	}


	/**
	 * @Route("/report/bilanAvoir", name="bilanAvoir")
	 */
	public function bilanAvoirIndex() {
		$request = Request::createFromGlobals();

		$from  = $request->request->get('from', !empty($_COOKIE["from"]) ? $_COOKIE["from"] : date("Y-m-01")) . " 00:00:00";
		$to  = $request->request->get('to', !empty($_COOKIE["to"]) ? $_COOKIE["to"] : date("Y-m-d")) . " 23:59:59";

		$_COOKIE["from"] = $from;
		$_COOKIE["to"] = $to;

		$dates = $this->formatFromTo($from, $to);

		$data = $this->getBilanAvoirReport($dates["from"], $dates["to"]);

		return $this->render('report/bilanAvoir/index.html.twig', [
			'data' => $data,
			'from' => $from,
			'to' => $to,
		]);
	}

	/**
	 * @Route("/report/bilanAvoir/export/from/{from}/to/{to}", name="exportBilanAvoir")
	 */
	public function exportBilanAvoir($from, $to) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getBilanAvoirReport($dates["from"], $dates["to"]);

		$response = new Response();
		$response->setContent("" . $this->renderView('report/bilanAvoir/export.csv.twig', [
				'data' => $data,
			]));
		$response->setCharset("utf-8");

		$response->headers->set('Content-Type', 'application/csv;');
		$response->headers->set('Content-Disposition', 'attachment; filename=rapport_bilan_avoir.csv');
		$response->headers->set('Pragma', 'no-cache');

		return $response;

	}

	protected function getBilanAvoirReport($from, $to) {

		$db = $this->getPDO();
		$baseQuery = $db->query(<<<SQL
SELECT
	core_store.name AS store_name,
	`dc`.`fr_name`,
    `dc`.`country_id`,
    `sfc`.`updated_at` AS `date`,
    `sfc`.`entity_id`,
    `sfc`.`increment_id` as `invoice_id`,
    `sfoi`.`item_id`,
    `sfci`.`name`,
    `sfci`.`sku`,
    `sfci`.`price_incl_tax`,
    `sfoi`.`original_price`,
    `sfo`.`created_at` AS `order_date`,
    `sfo`.`increment_id`,
	eaov.value as type_value
	
FROM sales_flat_creditmemo_item as sfci

LEFT JOIN sales_flat_order_item as sfoi ON (sfoi.item_id = sfci.order_item_id AND sfoi.parent_item_id IS NULL)
LEFT JOIN sales_flat_order as sfo ON sfoi.order_id = sfo.entity_id
LEFT JOIN sales_flat_creditmemo as sfc ON sfci.parent_id = sfc.entity_id
LEFT JOIN core_store ON core_store.store_id = sfo.store_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfo.shipping_address_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id
LEFT JOIN catalog_product_entity as cpe ON cpe.entity_id = sfci.product_id
LEFT JOIN eav_attribute as ea ON ea.attribute_code = "type"
LEFT JOIN catalog_product_entity_varchar as cpev ON cpev.attribute_id = ea.attribute_id AND cpev.entity_id = cpe.entity_id
LEFT JOIN catalog_product_entity_int as cpei ON cpei.attribute_id = ea.attribute_id AND cpei.entity_id = cpe.entity_id
LEFT JOIN eav_attribute_option_value as eaov ON eaov.option_id = IFNULL(cpei.value, cpev.value) AND eaov.store_id = 0

WHERE (sfc.updated_at >= '$from')
  AND (sfc.updated_at <= '$to')

GROUP BY sfoi.item_id

UNION

SELECT
	core_store.name AS store_name,
	`dc`.`fr_name`,
    `dc`.`country_id`,
    `sfc`.`updated_at` AS `date`,
    '' as `entity_id`,
    `sfc`.`increment_id` as `invoice_id`,
    '' as `item_id`,
    '' as `name`,
    '' as `sku`,
    sfc.base_grand_total as `price_incl_tax`,
    '' as `original_price`,
    `sfo`.`created_at` AS `order_date`,
    `sfo`.`increment_id`,
	'' as type_value
	
FROM sales_flat_creditmemo sfc

LEFT JOIN sales_flat_order_item as sfoi ON sfoi.order_id = sfc.order_id
LEFT JOIN sales_flat_order as sfo ON sfo.entity_id = sfc.order_id
LEFT JOIN sales_flat_creditmemo_item as sfci ON sfci.order_item_id = sfoi.item_id
LEFT JOIN core_store ON core_store.store_id = sfo.store_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfc.shipping_address_id
LEFT JOIN `directory_country` AS `dc` ON sfoa.country_id = dc.country_id

WHERE (sfc.updated_at >= '$from')
  AND (sfc.updated_at <= '$to')

GROUP BY sfc.entity_id

HAVING count(sfci.entity_id) = 0

SQL
		);

		$data = [];

		foreach($baseQuery as $q) {
			$data[] = [
				"store_name" => $q["store_name"],
				"fr_name" => $q["fr_name"],
				"country_id" => $q["country_id"],
				"date" => $q["date"],
				"item_id" => $q["item_id"],
				"increment_id" => $q["increment_id"],
				"invoice_id" => $q["invoice_id"],
				"order_date" => $q["order_date"],
				"name" => $q["name"],
				"sku" => $q["sku"],
				"price_incl_tax" => $q["price_incl_tax"],
				"original_price" => $q["original_price"],
				"type_value" => $q["type_value"]
			];
		}


		return $data;
	}


	/**
	 * @Route("/report/comptablePro", name="comptablePro")
	 */
	public function comptableProIndex() {
		$request = Request::createFromGlobals();

		$from  = $request->request->get('from', date("Y-m-01")) . " 00:00:00";
		$to  = date("Y-m-t", strtotime($from)) . " 23:59:59";
		$storeId  = $request->request->get('storeId', 0);
		$dates = $this->formatFromTo($from, $to);

		$data = $this->getComptableProReport($dates["from"], $dates["to"], $storeId);

		return $this->render('report/comptablePro/index.html.twig', [
			'data' => $data,
			'from' => $from,
			'storeId' => $storeId,
			'stores' => $this->getStores(8),
			'to' => $to
		]);
	}

	/**
	 * @Route("/report/comptablePro/export/from/{from}/to/{to}/store/{store}", name="exportComptablePro")
	 */
	public function exportComptablePro($from, $to, $store) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getComptableProReport($dates["from"], $dates["to"], $store);

		$response = new Response();
		$response->setContent("" . $this->renderView('report/comptablePro/export.csv.twig', [
				'data' => $data
			]));
		$response->setCharset("utf-8");

		$response->headers->set('Content-Type', 'application/csv;');
		$response->headers->set('Content-Disposition', 'attachment; filename=rapport_comptable_routine_expeditions.csv');
		$response->headers->set('Pragma', 'no-cache');

		return $response;

	}

	/**
	 * @Route("/report/comptablePro/excel/from/{from}/to/{to}/store/{store}", name="exportExcelComptablePro")
	 */
	public function exportExcelComptablePro($from, $to, $store) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getComptableProReport($dates["from"], $dates["to"], $store);
		
		$spreadsheet = new Spreadsheet();
		$columns = ["A" => ["label" => "Statut", "key" => "status"],
					"B" => ["label" => "Jour", "key" => "date"],
					"C" => ["label" => "Pièce", "key" => "piece"],
					"D" => ["label" => "Document", "key" => "document"],
					"E" => ["label" => "Compte général", "key" => "general_account"],
					"F" => ["label" => "Compte auxiliaire", "key" => "aux_account"],
					"G" => ["label" => "Libellé", "key" => "lib"],
					"H" => ["label" => "Débit", "key" => "debit"],
					"I" => ["label" => "Crédit", "key" => "credit"],
					"J" => ["label" => "Date de l'échéance", "key" => "end_date"],
					"K" => ["label" => "Poste analytique", "key" => "poste"],
					"L" => ["label" => "Documents associés", "key" => "associated_docs"]
		];
		
		/* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle("Rapport comptable PRO");

		$i = 1;
		foreach ($columns as $key => $column) {
			$cellValue = $key . $i;
			$sheet->setCellValue($cellValue, $column["label"]);
		}

		foreach ($data as $line) {
			$i++;
			$j = 0;
			foreach ($columns as $key => $column) {
				$cellValue = $key . $i;
				//die(var_dump($j));
				$sheet->setCellValue($cellValue, $line{$column["key"]});
				$j++;
			}
		}


		// Create your Office 2007 Excel (XLSX Format)
		$writer = new Xlsx($spreadsheet);

		// Create a Temporary file in the system
		$fileName = 'rapport_comptable_routine_expeditions.xlsx';
		$temp_file = tempnam(sys_get_temp_dir(), $fileName);

		// Create the excel file in the tmp directory of the system
		$writer->save($temp_file);

		// Return the excel file as an attachment
		return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
	}

	protected function getComptableProReport($from, $to, $store) {
		$data = [];

		$db = $this->getPDO();
		$baseQuery = $db->query(<<<SQL
SELECT `sfi`.`updated_at` AS `date`,
	   `sfi`.`increment_id`,
	   CONCAT('fact') AS `type`,
	   `sfi`.`grand_total`,
	   `sfi`.`tax_amount`,
	   `sfi`.`subtotal`,
	   `sfoa`.`firstname`,
	   `sfoa`.`lastname`,
	   `account_value`.`value` as `general_account`,
	   `company_value`.`value` as `company`
FROM `sales_flat_invoice` AS `sfi`
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfi.shipping_address_id
LEFT JOIN `eav_attribute` AS `account_attribute` ON account_attribute.attribute_code = 'general_account'
LEFT JOIN `customer_entity_varchar` AS `account_value` ON account_value.attribute_id = account_attribute.attribute_id AND account_value.entity_id = sfoa.customer_id
LEFT JOIN `eav_attribute` AS `company_attribute` ON company_attribute.attribute_code = 'company'
LEFT JOIN `customer_entity_varchar` AS `company_value` ON company_value.attribute_id = company_attribute.attribute_id AND company_value.entity_id = sfoa.customer_id 
WHERE (sfi.updated_at >= '$from')
  AND (sfi.updated_at <= '$to')
  AND sfi.store_id IN ($store)
GROUP BY `sfi`.`increment_id`
UNION
SELECT `sfc`.`updated_at` AS `date`,
       `sfc`.`increment_id`,
       CONCAT('avoir') AS `type`,
       `sfc`.`grand_total`,
       `sfc`.`tax_amount`,
       `sfc`.`subtotal`,
       `sfoa`.`firstname`,
       `sfoa`.`lastname`,
	   `account_value`.`value` as `general_account`,
	   `company_value`.`value` as `company`
FROM `sales_flat_creditmemo` AS `sfc`
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfc.shipping_address_id
LEFT JOIN `eav_attribute` AS `account_attribute` ON account_attribute.attribute_code = 'general_account'
LEFT JOIN `customer_entity_varchar` AS `account_value` ON account_value.attribute_id = account_attribute.attribute_id AND account_value.entity_id = sfoa.customer_id
LEFT JOIN `eav_attribute` AS `company_attribute` ON company_attribute.attribute_code = 'company'
LEFT JOIN `customer_entity_varchar` AS `company_value` ON company_value.attribute_id = company_attribute.attribute_id AND company_value.entity_id = sfoa.customer_id 
WHERE (sfc.updated_at >= '$from')
  AND (sfc.updated_at <= '$to')
  AND sfc.store_id IN ($store)
GROUP BY `sfc`.`increment_id`
ORDER BY date
SQL
		);

		foreach($baseQuery as $q) {

			$steps = [
				[
					"general_account" => $q["general_account"], // Data from client
					"value_to_display" => "grand_total",
					"value_as" => $q["type"] == "avoir" ? "credit" : "debit"
				], [
					"general_account" => "70711100",
					"value_to_display" => "subtotal",
					"value_as" => $q["type"] == "avoir" ? "debit" : "credit"
				], [
					"general_account" => "44571100",
					"value_to_display" => "tax_amount",
					"value_as" => $q["type"] == "avoir" ? "debit" : "credit"
				]
			];


			$q["status"] = 1;
			$q["date"] = date("d/m/Y", strtotime($q["date"] . "+2 hours"));
			$q["piece"] = $q["type"] . " " . $q["increment_id"];
			$q["document"] = "";
			$q["aux_account"] = "";
			$q["end_date"] = "";
			$q["poste"] = "";
			$q["associated_docs"] = "false";
			$q["lib"] = $q["company"] ?: $q["firstname"] . " " . $q["lastname"];

			foreach($steps as $step) {
				$line = $q;
				$line["debit"] = "";
				$line["credit"] = "";


				$line["general_account"] = $step["general_account"];
				$line{$step["value_as"]} = $q{$step["value_to_display"]};

				$data[] = $line;
			}
		}

		return $data;
	}






	/**
	 * @Route("/report/comptableProExpedition", name="comptableProExpedition")
	 */
	public function comptableProExpeditionIndex() {
		$request = Request::createFromGlobals();

		$from  = $request->request->get('from', date("Y-m-01")) . " 00:00:00";
		$to  = date("Y-m-t", strtotime($from)) . " 23:59:59";
		$storeId  = $request->request->get('storeId', 0);
		$dates = $this->formatFromTo($from, $to);

		$data = $this->getComptableProExpeditionReport($dates["from"], $dates["to"], $storeId);

		return $this->render('report/comptableProExpedition/index.html.twig', [
			'data' => $data,
			'from' => $from,
			'storeId' => $storeId,
			'stores' => $this->getStores(12),
			'to' => $to
		]);
	}

	/**
	 * @Route("/report/comptableProExpedition/export/from/{from}/to/{to}/store/{store}", name="exportComptableProExpedition")
	 */
	public function exportComptableProExpedition($from, $to, $store) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getComptableProExpeditionReport($dates["from"], $dates["to"], $store);

		$response = new Response();
		$response->setContent("" . $this->renderView('report/comptableProExpedition/export.csv.twig', [
				'data' => $data
			]));
		$response->setCharset("utf-8");

		$response->headers->set('Content-Type', 'application/csv;');
		$response->headers->set('Content-Disposition', 'attachment; filename=rapport_comptable_routine_expeditions.csv');
		$response->headers->set('Pragma', 'no-cache');

		return $response;

	}

	/**
	 * @Route("/report/comptableProExpedition/excel/from/{from}/to/{to}/store/{store}", name="exportExcelComptableProExpedition")
	 */
	public function exportExcelComptableProExpedition($from, $to, $store) {

		$dates = $this->formatFromTo($from, $to);
		$data = $this->getComptableProExpeditionReport($dates["from"], $dates["to"], $store);

		$spreadsheet = new Spreadsheet();
		$columns = ["A" => ["label" => "Statut", "key" => "status"],
					"B" => ["label" => "Jour", "key" => "date"],
					"C" => ["label" => "Pièce", "key" => "piece"],
					"D" => ["label" => "Document", "key" => "document"],
					"E" => ["label" => "Compte général", "key" => "general_account"],
					"F" => ["label" => "Compte auxiliaire", "key" => "aux_account"],
					"G" => ["label" => "Libellé", "key" => "lib"],
					"H" => ["label" => "Débit", "key" => "debit"],
					"I" => ["label" => "Crédit", "key" => "credit"],
					"J" => ["label" => "Date de l'échéance", "key" => "end_date"],
					"K" => ["label" => "Poste analytique", "key" => "poste"],
					"L" => ["label" => "Documents associés", "key" => "associated_docs"]
		];

		/* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle("Comptable PRO Expeditions");

		$i = 1;
		foreach ($columns as $key => $column) {
			$cellValue = $key . $i;
			$sheet->setCellValue($cellValue, $column["label"]);
		}

		foreach ($data as $line) {
			$i++;
			$j = 0;
			foreach ($columns as $key => $column) {
				$cellValue = $key . $i;
				//die(var_dump($j));
				$sheet->setCellValue($cellValue, $line{$column["key"]});
				$j++;
			}
		}


		// Create your Office 2007 Excel (XLSX Format)
		$writer = new Xlsx($spreadsheet);

		// Create a Temporary file in the system
		$fileName = 'rapport_comptable_routine_expeditions.xlsx';
		$temp_file = tempnam(sys_get_temp_dir(), $fileName);

		// Create the excel file in the tmp directory of the system
		$writer->save($temp_file);

		// Return the excel file as an attachment
		return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
	}

	protected function getComptableProExpeditionReport($from, $to, $store) {
		$data = [];

		$db = $this->getPDO();
		$baseQuery = $db->query(<<<SQL
SELECT `sfs`.`created_at` AS `date`,
	   `sfi`.`increment_id`,
	   CONCAT('fact') AS `type`,
	   `sfi`.`grand_total`,
	   `sfi`.`tax_amount`,
	   `sfi`.`subtotal`,
	   `sfoa`.`firstname`,
	   `sfoa`.`lastname`,
	   `company_value`.`value` as `company`
FROM `sales_flat_invoice` AS `sfi`
LEFT JOIN `sales_flat_order` AS `sfo` ON sfo.entity_id = sfi.order_id
LEFT JOIN `sales_flat_shipment` AS `sfs` ON sfs.order_id = sfo.entity_id
LEFT JOIN `sales_flat_order_address` AS `sfoa` ON sfoa.entity_id = sfi.shipping_address_id
LEFT JOIN `eav_attribute` AS `company_attribute` ON company_attribute.attribute_code = 'company'
LEFT JOIN `customer_entity_varchar` AS `company_value` ON company_value.attribute_id = company_attribute.attribute_id AND company_value.entity_id = sfoa.customer_id
WHERE (sfs.created_at >= '$from')
  AND (sfs.created_at <= '$to')
  AND sfi.store_id IN ($store)
GROUP BY `sfi`.`increment_id`
ORDER BY sfs.created_at
SQL
		);

		foreach($baseQuery as $q) {

			$steps = [
				[
					"general_account" => "401VTE",
					"value_to_display" => "grand_total",
					"value_as" => $q["type"] == "avoir" ? "credit" : "debit"
				], [
					"general_account" => "701000",
					"value_to_display" => "subtotal",
					"value_as" => $q["type"] == "avoir" ? "debit" : "credit"
				], [
					"general_account" => "445711",
					"value_to_display" => "tax_amount",
					"value_as" => $q["type"] == "avoir" ? "debit" : "credit"
				]
			];


			$q["status"] = 1;
			$q["date"] = date("d/m/Y", strtotime($q["date"] . "+2 hours"));
			$q["piece"] = $q["type"] . " " . $q["increment_id"];
			$q["document"] = "";
			$q["aux_account"] = "";
			$q["end_date"] = "";
			$q["poste"] = "";
			$q["associated_docs"] = "false";
			$q["lib"] = $q["company"] ?: $q["firstname"] . " " . $q["lastname"];

			foreach($steps as $step) {
				$line = $q;
				$line["debit"] = "";
				$line["credit"] = "";


				$line["general_account"] = $step["general_account"];
				$line{$step["value_as"]} = $q{$step["value_to_display"]};

				$data[] = $line;
			}
		}

		return $data;
	}


	protected function formatFromTo ($from, $to) {
		$from = date("Y-m-d H:i:s", strtotime($from . " - 2 hours"));
		$to = date("Y-m-d H:i:s", strtotime($to . " - 2 hours"));

		return ["from" => $from, "to" => $to];
	}

	protected function getPDO () {
    	return new PDO('mysql:host=127.0.0.1;dbname=magento1083;', "u1083", "mEtHv4ovcJJWXxNk");
    	//return new PDO('mysql:host=127.0.0.1:8889;dbname=modetic_194;', "root", "root");
	}

	protected function getStores ($group = []) {
		$stores = [];

		$db = $this->getPDO();
		$query = $db->query( "SELECT store_id, name FROM core_store" . ( $group ? " WHERE group_id IN ($group)" : "" ) );
		foreach ( $query as $q ) {
			$stores[] = $q;
		}

		return $stores;
	}
}
